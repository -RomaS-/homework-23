package com.stolbunov.roman.homework23;

import android.app.Activity;
import android.util.Log;

public final class Logger {

    private Logger() {
    }

    public static void d(String tag, String message, Activity activity) {
        Log.d(tag, message);
    }
}
