package com.stolbunov.roman.homework23.model;

public class ItemData {
    private int itemImage;
    private String itemTitle;
    private String itemText;
    private int itemBackground;

    public ItemData(int itemImage, String itemTitle, String itemText, int itemBackground) {
        this.itemImage = itemImage;
        this.itemTitle = itemTitle;
        this.itemText = itemText;
        this.itemBackground = itemBackground;
    }

    public int getItemImage() {
        return itemImage;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public String getItemText() {
        return itemText;
    }

    public int getItemBackground() {
        return itemBackground;
    }

    @Override
    public String toString() {
        return "ItemData{" +
                "itemImage=" + itemImage +
                ", itemTitle='" + itemTitle + '\'' +
                ", itemText='" + itemText + '\'' +
                ", itemBackground=" + itemBackground +
                '}';
    }
}
