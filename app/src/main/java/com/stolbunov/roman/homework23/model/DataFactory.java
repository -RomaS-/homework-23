package com.stolbunov.roman.homework23.model;


import android.graphics.Color;

import com.stolbunov.roman.homework23.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DataFactory {
    private static int dataCounter = 10;

    private static int itemImage = R.drawable.ic_android_green;
    private static String itemTitle = "Actions";
    private static String itemText = "Last used 5 hours ago";
    private static List<Integer> list;

    static {
        list = new ArrayList<>();
        list.add(Color.CYAN);
        list.add(Color.RED);
        list.add(Color.MAGENTA);
        list.add(Color.BLUE);
        list.add(Color.YELLOW);
        list.add(Color.CYAN);
        list.add(Color.RED);
        list.add(Color.MAGENTA);
        list.add(Color.BLUE);
        list.add(Color.YELLOW);
    }

    public static List<ItemData> createListData() {
        List<ItemData> listData = new LinkedList<>();
        while (dataCounter > 0) {
            listData.add(new ItemData(itemImage, itemTitle, itemText, list.get(dataCounter - 1)));
            dataCounter--;
        }
        return listData;
    }
}
