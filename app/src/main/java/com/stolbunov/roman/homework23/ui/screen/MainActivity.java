package com.stolbunov.roman.homework23.ui.screen;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.stolbunov.roman.homework23.Logger;
import com.stolbunov.roman.homework23.R;
import com.stolbunov.roman.homework23.model.DataFactory;
import com.stolbunov.roman.homework23.model.ItemData;
import com.stolbunov.roman.homework23.ui.adapters.RecyclerViewAdapter;
import com.stolbunov.roman.homework23.ui.layout_managers.RecyclerLayoutManager;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(DataFactory.createListData());
        adapter.setListener(this::onItemClickListener);
        RecyclerView.LayoutManager manager = RecyclerLayoutManager.getLayoutManager(this);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

    private void onItemClickListener(ItemData itemData) {
        Logger.d("tag", itemData.toString(), this);
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        CircleImageView imageView = findViewById(R.id.circle_image_view);
        imageView.setImageResource(R.drawable.ic_user);

        if (actionBar != null) {
            actionBar.setTitle(R.string.tools);
        }
    }
}
