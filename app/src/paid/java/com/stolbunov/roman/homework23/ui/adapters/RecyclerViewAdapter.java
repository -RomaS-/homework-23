package com.stolbunov.roman.homework23.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.stolbunov.roman.homework23.R;
import com.stolbunov.roman.homework23.model.ItemData;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {
    private List<ItemData> listData;
    private OnItemClickListener listener;

    public RecyclerViewAdapter(List<ItemData> listData) {
        this.listData = listData;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_recycler_linear, viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder recyclerViewHolder, int position) {
        recyclerViewHolder.bind(listData.get(position));
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView itemImage;
        AppCompatTextView itemTitle;
        AppCompatTextView itemText;
        LinearLayout itemContainer;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            itemImage = itemView.findViewById(R.id.ir_item_image);
            itemTitle = itemView.findViewById(R.id.ir_item_title);
            itemText = itemView.findViewById(R.id.ir_item_text);
            itemContainer = itemView.findViewById(R.id.ir_item_container);
        }

        public void bind(ItemData itemData) {
            itemContainer.setBackgroundColor(itemData.getItemBackground());
            itemImage.setImageResource(itemData.getItemImage());
            itemTitle.setText(itemData.getItemTitle());
            itemText.setText(itemData.getItemText());

            itemView.setOnClickListener(v -> onItemClick(itemData));
        }

        private void onItemClick(ItemData itemData) {
            if (listener != null) {
                listener.onItemClick(itemData);
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ItemData itemData);
    }
}
