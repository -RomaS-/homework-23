package com.stolbunov.roman.homework23.ui.layout_managers;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class RecyclerLayoutManager {
    public static RecyclerView.LayoutManager getLayoutManager(Context context) {
        return new GridLayoutManager(context,
                2,
                LinearLayoutManager.VERTICAL,
                false);
    }
}
